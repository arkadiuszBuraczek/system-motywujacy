//
//  HomeScreenCoordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 31/05/2021.
//

import UIKit
import FirebaseAuth

protocol HomeScreenCoordinatorProtocol: AnyObject {
    func logOut()
    func presentCreateTask()
    func presentEditTask()
    func presentCreateGoal()
    func presentEditGoal()
}

final class HomeScreenCoordinator: BaseCoordinator {
    private var homeScreenNavigationController: UINavigationController?
    private var viewController: HomeScreenViewController?
    
    func start() {
        let vc = HomeScreenViewController()
        vc.coordinatorDelegate = self
        
        let vm = HomeScreenViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
            self?.viewController = nil
        }
        
        vc.viewModel = vm
        viewController = vc

        navigationController?.pushViewController(vc, animated: false)
        
        if Auth.auth().currentUser == nil {
            showLoginCoordinator()
        }
    }
    
    private func showLoginCoordinator() {
        guard let navController = navigationController else { return }
        let coordinator = LoginCoordinator(navController)
        addChild(coordinator: coordinator)

        coordinator.deallocateIfNeeded = { [weak self, unowned coordinator] in
            self?.removeChild(coordinator: coordinator)
        }
        
        coordinator.start()
    }
    
    private func showPairCoordinator() {
        guard let navController = navigationController else { return }
        let coordinator = PairCoordinator(navController)
        addChild(coordinator: coordinator)
        
        coordinator.deallocateIfNeeded = { [weak self, unowned coordinator] in
            self?.removeChild(coordinator: coordinator)
        }
        
        coordinator.start()
    }
}

extension HomeScreenCoordinator: HomeScreenCoordinatorProtocol {
    func logOut() {
        do {
            try Auth.auth().signOut()
            dismissCurrentCoordinator()
            showLoginCoordinator()
        } catch {
            showLogoutErrorAlert()
            print("Error while signing out")
        }
    }
    
    func presentEditTask() {
        let vc = EditTaskViewController()
        vc.coordinatorDelegate = self
        
        let vm = EditTaskViewModelImplementation()
        vc.viewModel = vm
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func showLogoutErrorAlert() {
        
    }
    
    func dismissCurrentCoordinator() {
        
    }
    
    func presentCreateGoal() {
        let vc = CreateGoalViewController()
        vc.coordinatorDelegate = self
        
        let vm = CreateGoalViewModelImplementation()
        vc.viewModel = vm
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func presentEditGoal() {
        let vc = EditGoalViewController()
        vc.coordinatorDelegate = self
        
        let vm = EditGoalViewModelImplementation()
        vc.viewModel = vm
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func presentCreateTask() {
        let vc = CreateTaskViewController()
        vc.coordinatorDelegate = self
        
        let vm = CreateTaskViewModelImplementation()
        vc.viewModel = vm

        navigationController?.present(vc, animated: true, completion: nil)
    }
}
