//
//  RegisterCoordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 09/06/2021.
//

import UIKit

protocol RegisterCoordinatorProcol: Coordinator {
}

final class RegisterCoordinator: BaseCoordinator {
    private weak var parentCoordinator: Coordinator!
    
    func start() {
        let vm = RegisterViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
        }
        
        let vc = RegisterViewController(with: vm)
        vc.coordinatorDelegate = self
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    deinit {
        print("did deinit register coordnator")
    }
}

extension RegisterCoordinator: RegisterCoordinatorProcol {
    
}
