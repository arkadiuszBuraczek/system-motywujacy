//
//  RegisterCoordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit

protocol LoginCoordinatorProtocol: AnyObject {
    func navigateToRegister()
    func popToRoot()
}

final class LoginCoordinator: BaseCoordinator {
    private var viewController: LoginViewController?
    
    func start() {
        let vc = LoginViewController()
        vc.coordinatorDelegate = self
        
        let vm = LoginViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
        }
        vc.viewModel = vm
        viewController = vc

        navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginCoordinator: LoginCoordinatorProtocol {
    func navigateToRegister() {
        guard let navController = navigationController else { return }
        let registerCoordinator = RegisterCoordinator(navController)
        addChild(coordinator: registerCoordinator)
        registerCoordinator.deallocateIfNeeded = { [weak self, unowned registerCoordinator] in
            self?.removeChild(coordinator: registerCoordinator)
        }
        registerCoordinator.start()
    }
    
    func popToRoot() {
        navigationController?.popToRootViewController(animated: true)
    }
}
