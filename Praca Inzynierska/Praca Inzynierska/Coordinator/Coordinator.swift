//
//  Coordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 31/05/2021.
//

import UIKit

protocol Coordinator: AnyObject {
    func start()
}

class BaseCoordinator {
    weak var navigationController: UINavigationController?
    private var childCoordinators: [BaseCoordinator] = []
    var deallocateIfNeeded: (() -> Void)?
    
    let topViewController = UIApplication.shared.windows.first?.rootViewController
    
    func addChild(coordinator: BaseCoordinator) {
        childCoordinators.append(coordinator)
    }
    
    func removeChild(coordinator: BaseCoordinator) {
        childCoordinators.removeElements { $0 === coordinator }
    }

    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
}
