//
//  PairCoordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

protocol PairCoordinatorProtocol: AnyObject {
    func popToRoot()
    func showCreatePair()
    func showJoinPair()
}

final class PairCoordinator: BaseCoordinator {
    private var viewController: PairViewController?
    private var createPairViewController: CreatePairViewController?
    private var joinPairViewController: JoinPairViewController?
    
    func start() {
        let vc = PairViewController()
        vc.coordinatorDelegate = self
        
        let vm = PairViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
        }
        
        vc.viewModel = vm
        viewController = vc
        
        navigationController?.pushViewController(vc, animated: true)
    }
    

}

extension PairCoordinator: PairCoordinatorProtocol {
    func popToRoot() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func showJoinPair() {
        let vc = JoinPairViewController()
        vc.coordinatorDelegate = self
        
        let vm = JoinPairViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
        }
        
        vc.viewModel = vm
        joinPairViewController = vc
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func showCreatePair() {
        let vc = CreatePairViewController()
        vc.coordinatorDelegate = self
        
        let vm = CreatePairViewModelImplementation()
        vm.removeCoordinatorIfNeeded = { [weak self] in
            self?.deallocateIfNeeded?()
        }
        
        vc.viewModel = vm
        createPairViewController = vc
        
        navigationController?.present(vc, animated: true, completion: nil)
    }
}
