//
//  AppCoordinator.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 31/05/2021.
//

import UIKit

protocol AppCoordinatorProtocol: Coordinator {
    init(window: UIWindow?)
}

final class AppCoordinator: BaseCoordinator, AppCoordinatorProtocol {
    
    private weak var window: UIWindow?
    private var homeScreenCoordinator: HomeScreenCoordinatorProtocol?

    init(window: UIWindow?) {
        self.window = window
        let navController = UINavigationController()
        super.init(navController)
        window?.rootViewController = navController
    }
    
    func start() {
        showHomeScreenCoordinator()
    }
    
    private func showLoginCoordinator() {
        guard let navController = navigationController else { return }
        let coordinator = LoginCoordinator(navController)
        addChild(coordinator: coordinator)

        coordinator.deallocateIfNeeded = { [weak self, unowned coordinator] in
            self?.removeChild(coordinator: coordinator)
            self?.showHomeScreenCoordinator()
        }
        
        coordinator.start()
    }
    
    private func showHomeScreenCoordinator() {
        guard let navController = navigationController else { return }

        let coordinator = HomeScreenCoordinator(navController)
        addChild(coordinator: coordinator)

        coordinator.deallocateIfNeeded = { [weak self, unowned coordinator] in
            self?.removeChild(coordinator: coordinator)
        }
        
        coordinator.start()
    }
    
    deinit {
        print("Did deinit app coordinator")
    }
}
