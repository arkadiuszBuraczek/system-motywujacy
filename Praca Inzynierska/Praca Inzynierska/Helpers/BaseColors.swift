//
//  BaseColors.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit

struct BaseColor {
    static let background = UIColor(named: "background") ?? .white
    static let textGray = UIColor(named: "textGray") ?? .white
    static let baseButtonColor = UIColor(named: "baseButtonColor") ?? .white
    static let text = UIColor(named: "text") ?? .white
    static let textBlack = UIColor(named: "textBlack") ?? .white
    static let backgroundLight = UIColor(named: "backgroundLight") ?? .white
}
