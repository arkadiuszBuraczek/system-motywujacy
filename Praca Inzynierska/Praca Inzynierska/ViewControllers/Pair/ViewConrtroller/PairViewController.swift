//
//  PairViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

final class PairViewController: BaseViewController {
    
    weak var coordinatorDelegate: PairCoordinatorProtocol?
    private var homeView: PairView?
    var viewModel: PairViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = PairView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
    
    deinit {
        print("Did deinit login view controller")
    }
}

// MARK: - Private methods
private extension PairViewController {
    func configure() {
        
    }
}

// MARK: - PairViewDelegate
extension PairViewController: PairViewDelegate {
    func didTapOnJoinPairButton() {
        coordinatorDelegate?.showJoinPair()
    }
    
    func didTapOnCreatePairButton() {
        coordinatorDelegate?.showCreatePair()
    }
}

