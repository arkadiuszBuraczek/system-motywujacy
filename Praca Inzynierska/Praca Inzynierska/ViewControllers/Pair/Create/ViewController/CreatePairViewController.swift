//
//  CreatePairViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import Foundation

final class CreatePairViewController: BaseViewController {
    
    weak var coordinatorDelegate: PairCoordinatorProtocol?
    private var homeView: CreatePairView?
    var viewModel: CreatePairViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = CreatePairView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
    
    deinit {
        print("Did deinit create pair view controller")
    }
}

// MARK: - Private methods
private extension CreatePairViewController {
    func configure() {
        
    }
}

// MARK: - CreatePairViewDelegate
extension CreatePairViewController: CreatePairViewDelegate {
    
}

