//
//  CreatePairViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//


import Foundation
import FirebaseDatabase
import FirebaseAuth

protocol CreatePairViewModel: AnyObject {
    func createPair(with name: String, password: String)
}

final class CreatePairViewModelImplementation: BaseViewModel {
    private let ref = Database.database().reference(withPath: "pairs")
    private let dateFormatter = DateFormatter()
    private let defaults = UserDefaults()
    
    override init() {
        super.init()
    }
}

extension CreatePairViewModelImplementation: CreatePairViewModel {
    func createPair(with name: String, password: String) {
        let newPair = Pair(createDate: dateFormatter.string(from: Date()),
                           goals: [],
                           id: UUID().uuidString,
                           name: name,
                           tasks: [],
                           users: [User(name: Auth.auth().currentUser?.displayName ?? "", parent: true)],
                           password: password)
        
        self.ref.setValue(newPair)
        defaults.set(self.ref.child(newPair.id), forKey: "user_pair_ref")
    }
}

