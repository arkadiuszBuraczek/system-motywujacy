//
//  CreatePairView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

protocol CreatePairViewDelegate: AnyObject {
    
}

final class CreatePairView: UIView {
    weak var delegate: CreatePairViewDelegate?
    
    private let title = UILabel()
    private let descriptionLabel = UILabel()
    private let pairNameTextFieldDescription = UILabel()
    private let pairNameTextField = UITextField()
    private let passwordTextFieldInfoLabel = UILabel()
    private let passwordTextField = UITextField()
    private let showPasswordButton = UIButton()
    private let createPairButton = UIButton()
    
    private var shouldShowPassword = false

    convenience init() {
        self.init(frame: .zero)
        
        configuraView()
    }
}

private extension CreatePairView {
    func configuraView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureDescriptionLabel()
        configurePairNameTextField()
        configurePasswordTextField()
        configureCreatePairButton()
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Utwórz parę!"
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureDescriptionLabel() {
        addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.text = "Aby utworzyć parę należy podać jej nazwę oraz hasło.\nTworząc parę zostaniesz automatycznie oznaczony jako rodzic."
        descriptionLabel.font = .font(with: .regular, size: .medium)
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.textAlignment = .left
        descriptionLabel.textColor = .lightGray
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 32),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configurePairNameTextField() {
        addSubview(pairNameTextField)
        addSubview(pairNameTextFieldDescription)
        addSubview(showPasswordButton)
        
        showPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        pairNameTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        pairNameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        pairNameTextFieldDescription.text = "Nazwa pary"
        pairNameTextFieldDescription.textColor = BaseColor.textGray
        pairNameTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        pairNameTextField.borderStyle = .roundedRect
        
        
        NSLayoutConstraint.activate([
            pairNameTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            pairNameTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            pairNameTextField.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 36),
            
            pairNameTextFieldDescription.leadingAnchor.constraint(equalTo: pairNameTextField.leadingAnchor, constant: 4),
            pairNameTextFieldDescription.bottomAnchor.constraint(equalTo: pairNameTextField.topAnchor, constant: -6)
        ])
    }
    
    func configurePasswordTextField() {
        addSubview(passwordTextField)
        addSubview(passwordTextFieldInfoLabel)
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        
        passwordTextFieldInfoLabel.text = NSLocalizedString("password", comment: "")
        passwordTextFieldInfoLabel.textColor = BaseColor.textGray
        passwordTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        
        showPasswordButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        showPasswordButton.imageView?.contentMode = .scaleAspectFit
        showPasswordButton.addTarget(self, action: #selector(didTapOnShowPassword), for: .touchUpInside)

        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: pairNameTextField.bottomAnchor, constant: 36),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            
            passwordTextFieldInfoLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: 4),
            passwordTextFieldInfoLabel.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -6),
            
            showPasswordButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor),
            showPasswordButton.centerYAnchor.constraint(equalTo: passwordTextFieldInfoLabel.centerYAnchor),
            showPasswordButton.widthAnchor.constraint(equalToConstant: 22),
            showPasswordButton.heightAnchor.constraint(equalToConstant: 22)
        ])
    }
    
    func configureCreatePairButton() {
        addSubview(createPairButton)
        
        createPairButton.titleLabel?.font = .font(with: .semibold, size: .small)
        createPairButton.layer.cornerRadius = 8
        createPairButton.setTitle("Utwórz", for: .normal)
        createPairButton.addTarget(self, action: #selector(didTapOnCreatePairButton), for: .touchUpInside)
        createPairButton.backgroundColor = BaseColor.baseButtonColor

        createPairButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            createPairButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createPairButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createPairButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -60),
            createPairButton.heightAnchor.constraint(equalToConstant: 42)
        ])
    }
    
    // MARK: - selectors
    @objc
    func didTapOnShowPassword() {
        shouldShowPassword.toggle()
        
        let image = shouldShowPassword ? UIImage(systemName: "eye.slash.fill") : UIImage(systemName: "eye.fill")
        showPasswordButton.setImage(image, for: .normal)
        passwordTextField.isSecureTextEntry = !shouldShowPassword
    }
    
    @objc
    func didTapOnCreatePairButton() {
//        delegate?.didTapOnRegister()
    }
    
}

