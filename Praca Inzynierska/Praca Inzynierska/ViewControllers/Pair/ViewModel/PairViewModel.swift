//
//  PairViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import Foundation

protocol PairViewModel: AnyObject {
    
}

final class PairViewModelImplementation: BaseViewModel {
    
    override init() {
        
    }
}

extension PairViewModelImplementation: PairViewModel {
    
}
