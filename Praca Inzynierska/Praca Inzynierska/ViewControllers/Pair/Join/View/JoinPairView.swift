//
//  JoinPairView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

protocol JoinPairViewDelegate: AnyObject {
    
}

final class JoinPairView: UIView {
    weak var delegate: JoinPairViewDelegate?
    
    private let title = UILabel()
    private let descriptionLabel = UILabel()
    private let pairNameTextFieldDescription = UILabel()
    private let pairNameTextField = UITextField()
    private let passwordTextFieldInfoLabel = UILabel()
    private let passwordTextField = UITextField()
    private let showPasswordButton = UIButton()
    private let asParentLabel = UILabel()
    private let asParentSwitch = UISwitch()
    private let createPairButton = UIButton()

    
    private var shouldShowPassword = false

    convenience init() {
        self.init(frame: .zero)
        
        configuraView()
    }
}

private extension JoinPairView {
    func configuraView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureDescriptionLabel()
        configurePairNameTextField()
        configurePasswordTextField()
        configureAsParentLabel()
        configureAsParentSwitch()
        configureCreatePairButton()
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Dołącz do pary!"
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureDescriptionLabel() {
        addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.text = "Aby dołączyć do pary należy podać jej nazwę oraz hasło a następnie zaznaczyć czy konto ma widnieć jako Rodzic czy Dziecko."
        descriptionLabel.font = .font(with: .regular, size: .medium)
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.textAlignment = .left
        descriptionLabel.textColor = .lightGray
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 32),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configurePairNameTextField() {
        addSubview(pairNameTextField)
        addSubview(pairNameTextFieldDescription)
        addSubview(showPasswordButton)
        
        showPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        pairNameTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        pairNameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        pairNameTextFieldDescription.text = "Nazwa pary"
        pairNameTextFieldDescription.textColor = BaseColor.textGray
        pairNameTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        pairNameTextField.borderStyle = .roundedRect
        
        
        NSLayoutConstraint.activate([
            pairNameTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            pairNameTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            pairNameTextField.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 36),
            
            pairNameTextFieldDescription.leadingAnchor.constraint(equalTo: pairNameTextField.leadingAnchor, constant: 4),
            pairNameTextFieldDescription.bottomAnchor.constraint(equalTo: pairNameTextField.topAnchor, constant: -6)
        ])
    }
    
    func configurePasswordTextField() {
        addSubview(passwordTextField)
        addSubview(passwordTextFieldInfoLabel)
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        
        passwordTextFieldInfoLabel.text = NSLocalizedString("password", comment: "")
        passwordTextFieldInfoLabel.textColor = BaseColor.textGray
        passwordTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        
        showPasswordButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        showPasswordButton.imageView?.contentMode = .scaleAspectFit
        showPasswordButton.addTarget(self, action: #selector(didTapOnShowPassword), for: .touchUpInside)

        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: pairNameTextField.bottomAnchor, constant: 36),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            
            passwordTextFieldInfoLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: 4),
            passwordTextFieldInfoLabel.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -6),
            
            showPasswordButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor),
            showPasswordButton.centerYAnchor.constraint(equalTo: passwordTextFieldInfoLabel.centerYAnchor),
            showPasswordButton.widthAnchor.constraint(equalToConstant: 22),
            showPasswordButton.heightAnchor.constraint(equalToConstant: 22)
        ])
    }
    
    func configureAsParentLabel() {
        addSubview(asParentLabel)
        asParentLabel.translatesAutoresizingMaskIntoConstraints = false
        asParentLabel.text = "Rodzic"
        asParentLabel.font = .font(with: .semibold, size: .smallMedium)
        
        NSLayoutConstraint.activate([
            asParentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            asParentLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 16)
        ])
    }
    
    func configureAsParentSwitch() {
        addSubview(asParentSwitch)
        asParentSwitch.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            asParentSwitch.centerYAnchor.constraint(equalTo: asParentLabel.centerYAnchor),
            asParentSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureCreatePairButton() {
        addSubview(createPairButton)
        
        createPairButton.titleLabel?.font = .font(with: .semibold, size: .small)
        createPairButton.layer.cornerRadius = 8
        createPairButton.setTitle("Dołącz", for: .normal)
//        createPairButton.addTarget(self, action: #selector(didTapOnCreatePairButton), for: .touchUpInside)
        createPairButton.backgroundColor = BaseColor.baseButtonColor

        createPairButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            createPairButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createPairButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createPairButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -60),
            createPairButton.heightAnchor.constraint(equalToConstant: 42)
        ])
    }
    
    // MARK: - selectors
    @objc
    func didTapOnShowPassword() {
        shouldShowPassword.toggle()
        
        let image = shouldShowPassword ? UIImage(systemName: "eye.slash.fill") : UIImage(systemName: "eye.fill")
        showPasswordButton.setImage(image, for: .normal)
        passwordTextField.isSecureTextEntry = !shouldShowPassword
    }
}

