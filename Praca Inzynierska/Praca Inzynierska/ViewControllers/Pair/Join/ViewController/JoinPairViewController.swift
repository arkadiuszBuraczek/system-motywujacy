//
//  JoinPairViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

final class JoinPairViewController: BaseViewController {
    
    weak var coordinatorDelegate: PairCoordinatorProtocol?
    private var homeView: JoinPairView?
    var viewModel: JoinPairViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = JoinPairView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
    
    deinit {
        print("Did deinit login view controller")
    }
}

// MARK: - Private methods
private extension JoinPairViewController {
    func configure() {
        
    }
}

// MARK: - CreatePairViewDelegate
extension JoinPairViewController: JoinPairViewDelegate {
    
}

