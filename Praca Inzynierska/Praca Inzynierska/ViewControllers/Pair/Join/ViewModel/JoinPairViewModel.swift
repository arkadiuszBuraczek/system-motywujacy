//
//  JoinPairViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

protocol JoinPairViewModel: AnyObject {
    func joinPair(with name: String, password: String)
}

final class JoinPairViewModelImplementation: BaseViewModel {
    private let ref = Database.database().reference(withPath: "pairs")
    private let dateFormatter = DateFormatter()
    private let defaults = UserDefaults()
    private var userPair: Pair!
    
    override init() {
        
    }
}

extension JoinPairViewModelImplementation: JoinPairViewModel {
    func joinPair(with name: String, password: String) {
        ref.observe(.value) { [weak self] snapshot in
            for child in snapshot.children {
                if let snp = child as? DataSnapshot,
                   let pair = Pair(snapshot: snp) {
                    if pair.name == name, pair.password == password {
                        self?.userPair = pair
                        self?.defaults.set(self?.ref.child(pair.id), forKey: "user_pair_ref")
                    }
                }
            }
        }
    }
}
