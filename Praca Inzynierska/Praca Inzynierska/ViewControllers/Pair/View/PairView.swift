//
//  PairView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 16/06/2021.
//

import UIKit

protocol PairViewDelegate: AnyObject {
    func didTapOnCreatePairButton()
    func didTapOnJoinPairButton()
}

final class PairView: UIView {
    weak var delegate: PairViewDelegate?
    
    private let logo = UIImageView(image: UIImage(systemName: "checkmark.seal.fill"))
    private let title = UILabel()
    private let createPairButton = UIButton()
    private let joinPairButton = UIButton()
    
    convenience init() {
        self.init(frame: .zero)
        
        configuraView()
    }
}

private extension PairView {
    func configuraView() {
        backgroundColor = BaseColor.background
        configureLogo()
        configureTitle()
        configureCreatePairButton()
        configureJoinToPairButton()
    }
    
    func configureJoinToPairButton() {
        addSubview(joinPairButton)
        joinPairButton.translatesAutoresizingMaskIntoConstraints = false
        joinPairButton.setTitle("Dołącz do pary", for: .normal)
        joinPairButton.titleLabel?.font = .font(with: .semibold, size: .normal)
        joinPairButton.backgroundColor = BaseColor.baseButtonColor
        joinPairButton.layer.cornerRadius = 8
        joinPairButton.addTarget(self, action: #selector(didTapOnJoinPairButton), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            joinPairButton.topAnchor.constraint(equalTo: createPairButton.bottomAnchor, constant: 16),
            joinPairButton.heightAnchor.constraint(equalToConstant: 42),
            joinPairButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            joinPairButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    func configureCreatePairButton() {
        addSubview(createPairButton)
        createPairButton.translatesAutoresizingMaskIntoConstraints = false
        createPairButton.setTitle("Utwórz parę", for: .normal)
        createPairButton.titleLabel?.font = .font(with: .semibold, size: .normal)
        createPairButton.backgroundColor = BaseColor.baseButtonColor
        createPairButton.layer.cornerRadius = 8
        createPairButton.addTarget(self, action: #selector(didTapOnCreatePairButton), for: .touchUpInside)

        NSLayoutConstraint.activate([
            createPairButton.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 36),
            createPairButton.heightAnchor.constraint(equalToConstant: 42),
            createPairButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createPairButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Drogi uzytkowniku!\nNie masz utworzonej pary."
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 36),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureLogo() {
        addSubview(logo)
        logo.translatesAutoresizingMaskIntoConstraints = false
        logo.contentMode = .scaleAspectFill
        
        NSLayoutConstraint.activate([
            logo.topAnchor.constraint(equalTo: topAnchor, constant: 130),
            logo.centerXAnchor.constraint(equalTo: centerXAnchor),
            logo.widthAnchor.constraint(equalToConstant: 75),
            logo.heightAnchor.constraint(equalToConstant: 75)
        ])
    }
    
    // MARK: - Selectors
    @objc
    func didTapOnCreatePairButton() {
        delegate?.didTapOnCreatePairButton()
    }
    
    @objc
    func didTapOnJoinPairButton() {
        delegate?.didTapOnJoinPairButton()
    }
}
