//
//  CreateTaskViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 19/06/2021.
//

import Foundation

protocol CreateTaskViewModel: AnyObject {
    
}

final class CreateTaskViewModelImplementation: BaseViewModel {
    
    override init() {
        
    }
}

extension CreateTaskViewModelImplementation: CreateTaskViewModel {
    
}
