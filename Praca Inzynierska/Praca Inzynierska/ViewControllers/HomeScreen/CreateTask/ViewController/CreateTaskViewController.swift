//
//  CreateTaskViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 19/06/2021.
//

import UIKit

final class CreateTaskViewController: BaseViewController {
    weak var coordinatorDelegate: HomeScreenCoordinatorProtocol?
    private var homeView: CreateTaskView?
    var viewModel: CreateTaskViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = CreateTaskView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
}

private extension CreateTaskViewController {
    func configure() {
        
    }
}

extension CreateTaskViewController: CreateTaskViewDelegate {
    
}
