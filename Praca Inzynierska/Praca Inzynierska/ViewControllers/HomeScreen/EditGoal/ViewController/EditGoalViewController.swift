//
//  EditGoalViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import UIKit

final class EditGoalViewController: BaseViewController {
    weak var coordinatorDelegate: HomeScreenCoordinatorProtocol?
    private var homeView: EditGoalView?
    var viewModel: EditGoalViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = EditGoalView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
}

private extension EditGoalViewController {
    
}

extension EditGoalViewController: EditGoalViewDelegate {
    
}
