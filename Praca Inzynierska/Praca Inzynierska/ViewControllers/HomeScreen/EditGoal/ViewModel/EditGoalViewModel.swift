//
//  EditGoalViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import Foundation

protocol EditGoalViewModel: AnyObject {
    
}

final class EditGoalViewModelImplementation: BaseViewModel {
    override init() {
        
    }
}

extension EditGoalViewModelImplementation: EditGoalViewModel {
    
}
