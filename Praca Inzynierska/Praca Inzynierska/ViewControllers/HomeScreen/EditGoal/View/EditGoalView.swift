//
//  EditGoalView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import UIKit

protocol EditGoalViewDelegate: AnyObject {
    
}

final class EditGoalView: UIView {
    weak var delegate: EditGoalViewDelegate?
    
    private let title = UILabel()
    private let goalNameTextFieldDescription = UILabel()
    private let goalNameTextField = UITextField()
    private let goalPointsTextFieldDescription = UILabel()
    private let goalPointsTextField = UITextField()
    private let createTaskButton = UIButton()
    
    convenience init() {
        self.init(frame: .zero)
        
        configureView()
    }
}

private extension EditGoalView {
    func configureView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureTaskNameTextField()
        configureTaskPointsTextField()
        configureCreateGoalButton()
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Edytuj cel!"
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureTaskNameTextField() {
        addSubview(goalNameTextField)
        addSubview(goalNameTextFieldDescription)
        
        goalNameTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        goalNameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        goalNameTextFieldDescription.text = "Nazwa celu"
        goalNameTextFieldDescription.textColor = BaseColor.textGray
        goalNameTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        goalNameTextField.borderStyle = .roundedRect
        goalNameTextField.text = "Godzina gry na konsoli"
        
        NSLayoutConstraint.activate([
            goalNameTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            goalNameTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            goalNameTextField.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 36),
            
            goalNameTextFieldDescription.leadingAnchor.constraint(equalTo: goalNameTextField.leadingAnchor, constant: 4),
            goalNameTextFieldDescription.bottomAnchor.constraint(equalTo: goalNameTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureTaskPointsTextField() {
        addSubview(goalPointsTextField)
        addSubview(goalPointsTextFieldDescription)
        
        goalPointsTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        goalPointsTextField.translatesAutoresizingMaskIntoConstraints = false
        
        goalPointsTextFieldDescription.text = "Liczba wymaganych punktów do zdobycia"
        goalPointsTextFieldDescription.textColor = BaseColor.textGray
        goalPointsTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        goalPointsTextField.borderStyle = .roundedRect
        goalPointsTextField.placeholder = "Np. 20"
        goalPointsTextField.text = "50"
        goalPointsTextField.keyboardType = .numberPad
        
        NSLayoutConstraint.activate([
            goalPointsTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            goalPointsTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            goalPointsTextField.topAnchor.constraint(equalTo: goalNameTextField.bottomAnchor, constant: 36),
            
            goalPointsTextFieldDescription.leadingAnchor.constraint(equalTo: goalNameTextField.leadingAnchor, constant: 4),
            goalPointsTextFieldDescription.bottomAnchor.constraint(equalTo: goalPointsTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureCreateGoalButton() {
        addSubview(createTaskButton)
        
        createTaskButton.titleLabel?.font = .font(with: .semibold, size: .small)
        createTaskButton.layer.cornerRadius = 8
        createTaskButton.setTitle("Potwierdź zmiany", for: .normal)
        createTaskButton.backgroundColor = BaseColor.baseButtonColor

        createTaskButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            createTaskButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createTaskButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createTaskButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -60),
            createTaskButton.heightAnchor.constraint(equalToConstant: 42)
        ])
    }
}
