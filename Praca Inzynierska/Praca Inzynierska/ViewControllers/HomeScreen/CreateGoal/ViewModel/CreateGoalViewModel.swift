//
//  CreateGoalViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

protocol CreateGoalViewModel: AnyObject {
    func createGoal(with name: String, points: Int)
}

final class CreateGoalViewModelImplementation: BaseViewModel {
    private let ref = Database.database().reference(withPath: "pairs")
    var userPair: Pair!
    
    override init() {
        super.init()
        ref.observe(.value) { [weak self] snapshot in
            for child in snapshot.children {
                if let snp = child as? DataSnapshot,
                   let pair = Pair(snapshot: snp) {
                    if pair.users.map({ $0.name }).contains(Auth.auth().currentUser?.displayName) {
                        self?.userPair = pair
                    }
                }
            }
        }
    }
}

extension CreateGoalViewModelImplementation: CreateGoalViewModel {
    func createGoal(with name: String, points: Int) {
        let newGoal = Goal(currentValue: .zero, goalValue: points, name: name)
        
        userPair.goals.append(newGoal)
        let userPairRef = self.ref.child(userPair.id)
        userPairRef.setValue(userPair)
    }
}
