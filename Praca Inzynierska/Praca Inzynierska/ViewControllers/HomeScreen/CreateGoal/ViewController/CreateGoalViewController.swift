//
//  CreateGoalViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import UIKit

final class CreateGoalViewController: BaseViewController {
    weak var coordinatorDelegate: HomeScreenCoordinatorProtocol?
    private var homeView: CreateGoalView?
    var viewModel: CreateGoalViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = CreateGoalView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
}


private extension CreateGoalViewController {
    func configure() {
        
    }
}

extension CreateGoalViewController: CreateGoalViewDelegate {
    func didTapOnCreateTask(with name: String, points: Int) {
        viewModel?.createGoal(with: name, points: points)
    }
    
    
}
