//
//  CreateGoalView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 20/06/2021.
//

import UIKit

protocol CreateGoalViewDelegate: AnyObject {
    func didTapOnCreateTask(with name: String, points: Int)
}

final class CreateGoalView: UIView {
    private let title = UILabel()
    private let descriptionLabel = UILabel()
    private let goalNameTextFieldDescription = UILabel()
    private let goalNameTextField = UITextField()
    private let goalPointsTextFieldDescription = UILabel()
    private let goalPointsTextField = UITextField()
    private let createGoalButton = UIButton()
    
    weak var delegate: CreateGoalViewDelegate?
    
    convenience init() {
        self.init(frame: .zero)
        configureView()
    }
}

private extension CreateGoalView {
    func configureView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureDescriptionLabel()
        configureGoalNameTextField()
        configureGoalPointsTextField()
        configureCreateGoalButton()
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Utwórz cel!"
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureDescriptionLabel() {
        addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.text = "Aby utworzyć cel należy określić jego nazwę (nagrodę którą dziecko otrzyma) oraz ilość możliwych punktów wymaganych do zdobycia aby zrealizować cel."
        descriptionLabel.font = .font(with: .regular, size: .medium)
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.textAlignment = .left
        descriptionLabel.textColor = .lightGray
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 32),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureGoalNameTextField() {
        addSubview(goalNameTextField)
        addSubview(goalNameTextFieldDescription)
        
        goalNameTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        goalNameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        goalNameTextFieldDescription.text = "Nazwa celu"
        goalNameTextFieldDescription.textColor = BaseColor.textGray
        goalNameTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        goalNameTextField.borderStyle = .roundedRect
        goalNameTextField.placeholder = "Np. \"Godzina gry na konsoli\""
        
        NSLayoutConstraint.activate([
            goalNameTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            goalNameTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            goalNameTextField.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 36),
            
            goalNameTextFieldDescription.leadingAnchor.constraint(equalTo: goalNameTextField.leadingAnchor, constant: 4),
            goalNameTextFieldDescription.bottomAnchor.constraint(equalTo: goalNameTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureGoalPointsTextField() {
        addSubview(goalPointsTextField)
        addSubview(goalPointsTextFieldDescription)
        
        goalPointsTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        goalPointsTextField.translatesAutoresizingMaskIntoConstraints = false
        
        goalPointsTextFieldDescription.text = "Liczba wymaganych punktów do zdobycia"
        goalPointsTextFieldDescription.textColor = BaseColor.textGray
        goalPointsTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        goalPointsTextField.borderStyle = .roundedRect
        goalPointsTextField.placeholder = "Np. 20"
        goalPointsTextField.keyboardType = .numberPad
        
        NSLayoutConstraint.activate([
            goalPointsTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            goalPointsTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            goalPointsTextField.topAnchor.constraint(equalTo: goalNameTextField.bottomAnchor, constant: 36),
            
            goalPointsTextFieldDescription.leadingAnchor.constraint(equalTo: goalNameTextField.leadingAnchor, constant: 4),
            goalPointsTextFieldDescription.bottomAnchor.constraint(equalTo: goalPointsTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureCreateGoalButton() {
        addSubview(createGoalButton)
        
        createGoalButton.titleLabel?.font = .font(with: .semibold, size: .small)
        createGoalButton.layer.cornerRadius = 8
        createGoalButton.setTitle("Utwórz cel", for: .normal)
        createGoalButton.backgroundColor = BaseColor.baseButtonColor

        createGoalButton.translatesAutoresizingMaskIntoConstraints = false
        createGoalButton.addTarget(self, action: #selector(didTapOnCreateGoal), for: .touchUpInside)
        NSLayoutConstraint.activate([
            createGoalButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createGoalButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createGoalButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -60),
            createGoalButton.heightAnchor.constraint(equalToConstant: 42)
        ])
    }
    
    @objc
    func didTapOnCreateGoal() {
        self.delegate?.didTapOnCreateTask(with: goalNameTextField.text ?? "",
                                          points: Int(goalPointsTextField.text ?? "") ?? .zero)
    }
}
