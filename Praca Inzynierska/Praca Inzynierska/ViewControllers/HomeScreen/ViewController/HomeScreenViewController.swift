//
//  HomeScreenViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 31/05/2021.
//

import UIKit
import FirebaseDatabase

final class HomeScreenViewController: BaseViewController {
    
    weak var coordinatorDelegate: HomeScreenCoordinatorProtocol?
    var viewModel: HomeScreenViewModel?
    private var homeView: HomeScreenView?
    private let defaults = UserDefaults()
    private var pairRef: DatabaseReference!
    private var userPair: Pair!

    override func loadView() {
        let mainView = HomeScreenView()
        self.homeView = mainView
        self.homeView?.delegate = self
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
//        pairRef = Database.database().reference(withPath: defaults.value(forKey: "user_pair_ref") as! String)
        
//        pairRef.observe(.value) { [weak self] snapshot in
//            // Will be one element/object
//            for child in snapshot.children {
//                if let snp = child as? DataSnapshot,
//                   let pair = Pair(snapshot: snp) {
//                    self?.userPair = pair
//                }
//            }
//        }
    }
}

// MARK: - Private methods
private extension HomeScreenViewController {
    func setup() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "arrow.right.circle.fill"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(didTapOnLogOut))
    }
    
    
    // MARK: - Selectors
    @objc
    func didTapOnLogOut() {
        let alert = UIAlertController(title: "Uwaga",
                                      message: "Czy chcesz się wylogować?",
                                      preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Potwierdź",
                                          style: .default) { [weak self] _ in
            self?.coordinatorDelegate?.logOut()
        }
        
        let cancelAction = UIAlertAction(title: "Anuluj",
                                         style: .cancel,
                                         handler: nil)
        
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func didTapOnRemoveTask() {
        let alertController = UIAlertController(title: "", message: "Czy jesteś pewien/pewna, że chcesz usunąć to zadanie?", preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Potwierdź", style: .default, handler: nil)
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        alertController.addAction(confirm)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension HomeScreenViewController: HomeScreenViewDelegate {
    
    func didTapOnConfirmTaskFinished() {
//        let alert = UIAlertController(title: "Uwaga", message: "Czy chcesz potwierdzić zadanie \"Wynieść śmieci\" jako wykonane?\nPunkty za to zadanie zostaną automatycznie doliczone do aktualnych celów.", preferredStyle: .alert)
//        let confirm = UIAlertAction(title: "Potwierdź", style: .default, handler: nil)
//        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
//
//        alert.addAction(confirm)
//        alert.addAction(cancel)
//
//        present(alert, animated: true, completion: nil)
        let alert = UIAlertController(title: "Uwaga!", message: "Czy chcesz zatwierdzić odebranie nagrody \"Godzina gry na konsoli\"?", preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Potwierdź", style: .default, handler: nil)
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        
        alert.addAction(confirm)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func didTapOnAssigment() {        
        let alert = UIAlertController(title: "Uwaga", message: "Czy chcesz oznaczyć zadanie \"Odkurzyć pokój\" jako wykonane?", preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Potwierdź", style: .default, handler: nil)
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        
        alert.addAction(confirm)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func didTapOnAddChildGoalButton() {
        coordinatorDelegate?.presentCreateGoal()
    }
    
    func didTapOnEdidChildGoal() {
        coordinatorDelegate?.presentEditTask()
    }
    
    func didTapOnAddAssigment() {
        coordinatorDelegate?.presentCreateTask()
    }
    
    func didTapOnEditGoal() {
        let alertController = UIAlertController(title: "", message: "Wybierz jaką akcję chcesz wykonać z zaznaczonym celem", preferredStyle: .actionSheet)
        let edit = UIAlertAction(title: "Edycja", style: .default, handler: { [weak self] _ in
            self?.coordinatorDelegate?.presentEditGoal()
        })
        let remove = UIAlertAction(title: "Usuń", style: .default, handler: nil)
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        alertController.addAction(edit)
        alertController.addAction(remove)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func didTapOnEditAssigment() {
        let alertController = UIAlertController(title: "", message: "Wybierz jaką akcję chcesz wykonać z zaznaczonym zadaniem", preferredStyle: .actionSheet)
        let edit = UIAlertAction(title: "Edycja", style: .default, handler: { [weak self] _ in
            self?.coordinatorDelegate?.presentEditTask()
        })
        let remove = UIAlertAction(title: "Usuń", style: .default, handler: { [weak self] _ in
            self?.didTapOnRemoveTask()
        })
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        alertController.addAction(edit)
        alertController.addAction(remove)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func didTapOnGainGoal() {
        let alert = UIAlertController(title: "Uwaga!", message: "Czy chcesz zatwierdzić odebranie nagrody \"Godzina gry na konsoli\"?", preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Potwierdź", style: .default, handler: nil)
        let cancel = UIAlertAction(title: "Anuluj", style: .cancel, handler: nil)
        
        alert.addAction(confirm)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func removeGoal(index: IndexPath) {
        userPair.goals.remove(at: index.row)
        pairRef.setValue(userPair)
    }
}
