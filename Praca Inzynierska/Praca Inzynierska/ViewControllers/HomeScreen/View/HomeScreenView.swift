//
//  HomeScreenView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 31/05/2021.
//

import UIKit


enum UserType {
    case parent, child
}

protocol HomeScreenViewDelegate: AnyObject {
    func didTapOnAddChildGoalButton()
    func didTapOnEdidChildGoal()
    func didTapOnEditAssigment()
    func didTapOnAddAssigment()
    func didTapOnAssigment()
    func didTapOnConfirmTaskFinished()
    func didTapOnEditGoal()
    func didTapOnGainGoal()
}

final class HomeScreenView: UIView {
    
    weak var delegate: HomeScreenViewDelegate?
    private let userType: UserType = .parent
    
    convenience init() {
        self.init(frame: .zero)
        setup()
    }
}

private extension HomeScreenView {
    func setup() {
        backgroundColor = BaseColor.background
        
        switch userType {
        case .parent:
            setupParentUI()
        case .child:
            setupChildUI()
        }
    }
    
    func setupParentUI() {
        // Child goals section
        let childGoalsSectionTitle = UILabel()
        let childGoalsSectionDesc = UILabel()
        let addChildGoalButton = UIButton()
        let goalHeader = UILabel()
        let firstGoalLabel = UILabel()
        let firstGoalEditButton = UIButton()
        let firstSeparator = UIView()
        
        [childGoalsSectionTitle, childGoalsSectionDesc, addChildGoalButton,
         addChildGoalButton, firstGoalLabel, firstGoalEditButton,
        firstSeparator, goalHeader].forEach({
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
        
        goalHeader.text = NSLocalizedString("current_goals_header", comment: "")
        goalHeader.font = .font(with: .semibold, size: .smallMedium)
        goalHeader.textColor = .lightGray
        
        firstSeparator.backgroundColor = .lightGray
        childGoalsSectionTitle.text = NSLocalizedString("child_goals_section_title", comment: "")
        childGoalsSectionTitle.textColor = BaseColor.textBlack
        childGoalsSectionTitle.font = .font(with: .semibold, size: .medium)
        
        childGoalsSectionDesc.text = NSLocalizedString("child_goals_section_desc", comment: "")
        childGoalsSectionDesc.font = .font(with: .regular, size: .smallerMedium)
        childGoalsSectionDesc.numberOfLines = .zero
        childGoalsSectionDesc.textColor = .lightGray

        addChildGoalButton.setImage(UIImage(systemName: "plus.circle"), for: .normal)
        addChildGoalButton.addTarget(self, action: #selector(didTapOnAddChildGoalButton), for: .touchUpInside)
        
        firstGoalLabel.text = "Godzina gry na konsoli    (50/50)"
        firstGoalLabel.textColor = BaseColor.textBlack
        firstGoalLabel.font = .font(with: .regular, size: .small)

        firstGoalEditButton.setImage(UIImage(systemName: "pencil"), for: .normal)
        firstGoalEditButton.addTarget(self, action: #selector(didTapOnEditGoal), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            childGoalsSectionTitle.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            childGoalsSectionTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            
            addChildGoalButton.heightAnchor.constraint(equalToConstant: 25),
            addChildGoalButton.widthAnchor.constraint(equalToConstant: 25),
            addChildGoalButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            addChildGoalButton.centerYAnchor.constraint(equalTo: childGoalsSectionTitle.centerYAnchor),
            
            childGoalsSectionDesc.leadingAnchor.constraint(equalTo: childGoalsSectionTitle.leadingAnchor),
            childGoalsSectionDesc.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            childGoalsSectionDesc.topAnchor.constraint(equalTo: childGoalsSectionTitle.bottomAnchor, constant: 8),
            
            goalHeader.topAnchor.constraint(equalTo: childGoalsSectionDesc.bottomAnchor, constant: 28),
            goalHeader.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            
            firstGoalLabel.leadingAnchor.constraint(equalTo: childGoalsSectionTitle.leadingAnchor),
            firstGoalLabel.topAnchor.constraint(equalTo: goalHeader.bottomAnchor, constant: 8),
            
            firstGoalEditButton.centerYAnchor.constraint(equalTo: firstGoalLabel.centerYAnchor),
            firstGoalEditButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            firstGoalEditButton.heightAnchor.constraint(equalToConstant: 25),
            firstGoalEditButton.widthAnchor.constraint(equalToConstant: 25),
            
            firstSeparator.heightAnchor.constraint(equalToConstant: 1),
            firstSeparator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            firstSeparator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            firstSeparator.topAnchor.constraint(equalTo: firstGoalEditButton.bottomAnchor, constant: 16)
        ])
        
        // Assigments
        let childAssigmentsSectionTitle = UILabel()
        let childAssigmentsSectionDesc = UILabel()
        let addChildAssigmentButton = UIButton()
        let assigmentHeader = UILabel()
        let firstAssigmentLabel = UILabel()
        let secondAssigmentLabel = UILabel()
        let thirdAssigmentLabel = UILabel()
        let fourthAssigmentLabel = UILabel()
        let firstAssigmentEditButton = UIButton()
        let secondAssigmentEditButton = UIButton()
        let thirdAssigmentEditButton = UIButton()
        let fourthAssigmentEditButton = UIButton()
        let secondSeparator = UIView()
        
        [childAssigmentsSectionTitle, childAssigmentsSectionDesc, addChildAssigmentButton,
         assigmentHeader, firstAssigmentLabel, secondAssigmentLabel,
         thirdAssigmentLabel, fourthAssigmentLabel, firstAssigmentEditButton,
         secondAssigmentEditButton, thirdAssigmentEditButton, fourthAssigmentEditButton,
         secondSeparator].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        assigmentHeader.text = NSLocalizedString("child_assigment_section_title", comment: "")
        assigmentHeader.font = .font(with: .semibold, size: .smallMedium)
        assigmentHeader.textColor = .lightGray
        
        secondSeparator.backgroundColor = .lightGray
        childAssigmentsSectionTitle.text = NSLocalizedString("child_assigment_section_title", comment: "")
        childAssigmentsSectionTitle.textColor = BaseColor.textBlack
        childAssigmentsSectionTitle.font = .font(with: .semibold, size: .medium)
        
        childAssigmentsSectionDesc.text = NSLocalizedString("child_assigment_section_desc", comment: "")
        childAssigmentsSectionDesc.font = .font(with: .regular, size: .smallerMedium)
        childAssigmentsSectionDesc.numberOfLines = .zero
        childAssigmentsSectionDesc.textColor = .lightGray

        addChildAssigmentButton.setImage(UIImage(systemName: "plus.circle"), for: .normal)
        addChildAssigmentButton.addTarget(self, action: #selector(didTapOnAddChildAssigmentButton), for: .touchUpInside)
        
        firstAssigmentLabel.text = "Odrobić lekcje  15pkt"//NSLocalizedString("child_assigment_1", comment: "")
        firstAssigmentLabel.textColor = BaseColor.textBlack
        firstAssigmentLabel.font = .font(with: .regular, size: .small)
        
        secondAssigmentLabel.text = "Posprzątać po obiedzie   5pkt"//NSLocalizedString("child_assigment_2", comment: "")
        secondAssigmentLabel.textColor = BaseColor.textBlack
        secondAssigmentLabel.font = .font(with: .regular, size: .small)
        
        thirdAssigmentLabel.text = "Wyprowadzić psa    5pkt"//NSLocalizedString("child_assigment_3", comment: "")
        thirdAssigmentLabel.textColor = BaseColor.textBlack
        thirdAssigmentLabel.font = .font(with: .regular, size: .small)
        
        let alertImage = UIImage(systemName: "bell.fill")
        let imgView = UIImageView(image: alertImage)
        imgView.tintColor = .orange
        imgView.contentMode = .scaleAspectFit
        imgView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imgView)
        
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnConfirmTaskFinished)))
        
        fourthAssigmentLabel.text = "Wywiesić pranie    5pkt"//NSLocalizedString("child_assigment_4", comment: "")
        fourthAssigmentLabel.textColor = BaseColor.textBlack
        fourthAssigmentLabel.font = .font(with: .regular, size: .small)
        
        firstAssigmentEditButton.addTarget(self, action: #selector(didTapOnFirstAssigmentButton), for: .touchUpInside)
//        checkmark.seal.fill
        firstAssigmentEditButton.setImage(UIImage(systemName: "checkmark.seal"), for: .normal)
        secondAssigmentEditButton.setImage(UIImage(systemName: "checkmark.seal"), for: .normal)
        thirdAssigmentEditButton.setImage(UIImage(systemName: "checkmark.seal"), for: .normal)
        fourthAssigmentEditButton.setImage(UIImage(systemName: "checkmark.seal"), for: .normal)
        
        NSLayoutConstraint.activate([
            childAssigmentsSectionTitle.topAnchor.constraint(equalTo: firstSeparator.bottomAnchor, constant: 16),
            childAssigmentsSectionTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            
            addChildAssigmentButton.heightAnchor.constraint(equalToConstant: 25),
            addChildAssigmentButton.widthAnchor.constraint(equalToConstant: 25),
            addChildAssigmentButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            addChildAssigmentButton.centerYAnchor.constraint(equalTo: childAssigmentsSectionTitle.centerYAnchor),
            
            childAssigmentsSectionDesc.leadingAnchor.constraint(equalTo: childGoalsSectionTitle.leadingAnchor),
            childAssigmentsSectionDesc.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            childAssigmentsSectionDesc.topAnchor.constraint(equalTo: childAssigmentsSectionTitle.bottomAnchor, constant: 8),
            
            assigmentHeader.topAnchor.constraint(equalTo: childAssigmentsSectionDesc.bottomAnchor, constant: 28),
            assigmentHeader.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            
            firstAssigmentLabel.leadingAnchor.constraint(equalTo: firstAssigmentEditButton.trailingAnchor, constant: 16),
            firstAssigmentLabel.topAnchor.constraint(equalTo: assigmentHeader.bottomAnchor, constant: 8),
            
            firstAssigmentEditButton.centerYAnchor.constraint(equalTo: firstAssigmentLabel.centerYAnchor),
            firstAssigmentEditButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            firstAssigmentEditButton.heightAnchor.constraint(equalToConstant: 25),
            firstAssigmentEditButton.widthAnchor.constraint(equalToConstant: 25),
            
            secondAssigmentLabel.leadingAnchor.constraint(equalTo: secondAssigmentEditButton.trailingAnchor, constant: 16),
            secondAssigmentLabel.topAnchor.constraint(equalTo: firstAssigmentLabel.bottomAnchor, constant: 16),
            
            secondAssigmentEditButton.centerYAnchor.constraint(equalTo: secondAssigmentLabel.centerYAnchor),
            secondAssigmentEditButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            secondAssigmentEditButton.heightAnchor.constraint(equalToConstant: 25),
            secondAssigmentEditButton.widthAnchor.constraint(equalToConstant: 25),
            
            thirdAssigmentLabel.leadingAnchor.constraint(equalTo: thirdAssigmentEditButton.trailingAnchor, constant: 16),
            thirdAssigmentLabel.topAnchor.constraint(equalTo: secondAssigmentLabel.bottomAnchor, constant: 16),
            
            imgView.centerYAnchor.constraint(equalTo: firstGoalEditButton.centerYAnchor),
            imgView.trailingAnchor.constraint(equalTo: firstGoalEditButton.leadingAnchor, constant: -8),
            imgView.widthAnchor.constraint(equalToConstant: 20),
            imgView.heightAnchor.constraint(equalToConstant: 20),
            
            thirdAssigmentEditButton.centerYAnchor.constraint(equalTo: thirdAssigmentLabel.centerYAnchor),
            thirdAssigmentEditButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            thirdAssigmentEditButton.heightAnchor.constraint(equalToConstant: 25),
            thirdAssigmentEditButton.widthAnchor.constraint(equalToConstant: 25),
            
            fourthAssigmentLabel.leadingAnchor.constraint(equalTo: fourthAssigmentEditButton.trailingAnchor, constant: 16),
            fourthAssigmentLabel.topAnchor.constraint(equalTo: thirdAssigmentLabel.bottomAnchor, constant: 16),
            
            fourthAssigmentEditButton.centerYAnchor.constraint(equalTo: fourthAssigmentLabel.centerYAnchor),
            fourthAssigmentEditButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            fourthAssigmentEditButton.heightAnchor.constraint(equalToConstant: 25),
            fourthAssigmentEditButton.widthAnchor.constraint(equalToConstant: 25),
        ])
    }
    
    func setupChildUI() {
        // Title
        let title = UILabel()
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        
        title.text = "Obecne cele"
        title.font = .font(with: .semibold, size: .normal)
        title.textColor = .black
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
        
        // Goals
        let pointsLabel = UILabel()
        addSubview(pointsLabel)
        pointsLabel.text = "Godzina gry na konsoli    (50/50)"
        pointsLabel.textColor = BaseColor.textBlack
        pointsLabel.font = .font(with: .regular, size: .small)
        pointsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let progressView = UIProgressView(progressViewStyle: .bar)
        addSubview(progressView)
        progressView.progress = 1
        progressView.trackTintColor = .lightGray.withAlphaComponent(0.4)
        progressView.layer.cornerRadius = 10
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 10
        progressView.subviews[1].clipsToBounds = true
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.tintColor = .systemGreen
        
        let percentLabel = UILabel()
        percentLabel.text = "100%"
        percentLabel.font = .font(with: .semibold, size: .smaller)
        percentLabel.translatesAutoresizingMaskIntoConstraints = false
        percentLabel.textColor = .white
        addSubview(percentLabel)
        
        let alertImage = UIImage(systemName: "checkmark.seal.fill")
        let imgView = UIImageView(image: alertImage)
        imgView.tintColor = .systemGreen
        imgView.contentMode = .scaleAspectFit
        imgView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imgView)
        
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnGainGoal)))
        
        NSLayoutConstraint.activate([
            pointsLabel.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 24),
            pointsLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            pointsLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            progressView.topAnchor.constraint(equalTo: pointsLabel.bottomAnchor, constant: 12),
            progressView.leadingAnchor.constraint(equalTo: pointsLabel.leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: pointsLabel.trailingAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 20),
            
            percentLabel.leadingAnchor.constraint(equalTo: progressView.leadingAnchor, constant: 8),
            percentLabel.centerYAnchor.constraint(equalTo: progressView.centerYAnchor),
            
            imgView.centerYAnchor.constraint(equalTo: pointsLabel.centerYAnchor),
            imgView.trailingAnchor.constraint(equalTo: progressView.trailingAnchor)
        ])
        
        // Assigments
        let assigmentsTitle = UILabel()
        assigmentsTitle.translatesAutoresizingMaskIntoConstraints = false
        addSubview(assigmentsTitle)
        
        assigmentsTitle.text = "Obecne zadania"
        assigmentsTitle.font = .font(with: .semibold, size: .normal)
        assigmentsTitle.textColor = .black
        
        NSLayoutConstraint.activate([
            assigmentsTitle.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 32),
            assigmentsTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            assigmentsTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
        
        let assigmentsDesc = UILabel()
        assigmentsDesc.translatesAutoresizingMaskIntoConstraints = false
        addSubview(assigmentsDesc)
        assigmentsDesc.font = .font(with: .regular, size: .smallerMedium)
        assigmentsDesc.numberOfLines = .zero
        assigmentsDesc.textColor = .lightGray
        assigmentsDesc.text = "Po zrealizowaniu zadania kliknij na nie oraz potwierdź jego wykonanie"
        
        NSLayoutConstraint.activate([
            assigmentsDesc.topAnchor.constraint(equalTo: assigmentsTitle.bottomAnchor, constant: 8),
            assigmentsDesc.leadingAnchor.constraint(equalTo: assigmentsTitle.leadingAnchor),
            assigmentsDesc.trailingAnchor.constraint(equalTo: assigmentsTitle.trailingAnchor)
        ])
        
        
        let firstAssigmentLabel = UILabel()
        let secondAssigmentLabel = UILabel()
        let thirdAssigmentLabel = UILabel()
        let fourthAssigmentLabel = UILabel()
        
        [firstAssigmentLabel, secondAssigmentLabel, thirdAssigmentLabel, fourthAssigmentLabel].forEach { lbl in
            lbl.translatesAutoresizingMaskIntoConstraints = false
            addSubview(lbl)
        }
        
        let standardAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.font(with: .regular, size: .small), .foregroundColor: BaseColor.textBlack]
        let semiboldAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.font(with: .semibold, size: .small), .foregroundColor: BaseColor.textBlack]
        
        let firstAttrStr = NSMutableAttributedString(string: "Odrobić lekcje  ", attributes: standardAttributes)
        firstAttrStr.append(NSAttributedString(string: "15pkt", attributes: semiboldAttributes))
        
        firstAssigmentLabel.attributedText = firstAttrStr
        firstAssigmentLabel.isUserInteractionEnabled = true
        firstAssigmentLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnAssigment)))

        let secondAttrStr = NSMutableAttributedString(string: "Posprzątać po obiedzie   ", attributes: standardAttributes)
        secondAttrStr.append(NSAttributedString(string: "5pkt", attributes: semiboldAttributes))
        
        secondAssigmentLabel.attributedText = secondAttrStr
        
        let thirdAttrStr = NSMutableAttributedString(string: "Wyprowadzić psa    ", attributes: standardAttributes)
        thirdAttrStr.append(NSAttributedString(string: "5pkt", attributes: semiboldAttributes))
        
        thirdAssigmentLabel.attributedText = thirdAttrStr
        
        let fourthAttrString = NSMutableAttributedString(string: "Wywiesić pranie    ", attributes: standardAttributes)
        fourthAttrString.append(NSAttributedString(string: "5pkt", attributes: semiboldAttributes))
        
        fourthAssigmentLabel.attributedText = fourthAttrString
        
        NSLayoutConstraint.activate([
            firstAssigmentLabel.topAnchor.constraint(equalTo: assigmentsDesc.bottomAnchor, constant: 20),
            firstAssigmentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            firstAssigmentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            secondAssigmentLabel.topAnchor.constraint(equalTo: firstAssigmentLabel.bottomAnchor, constant: 20),
            secondAssigmentLabel.leadingAnchor.constraint(equalTo: firstAssigmentLabel.leadingAnchor),
            secondAssigmentLabel.trailingAnchor.constraint(equalTo: firstAssigmentLabel.trailingAnchor),
            
            thirdAssigmentLabel.topAnchor.constraint(equalTo: secondAssigmentLabel.bottomAnchor, constant: 20),
            thirdAssigmentLabel.leadingAnchor.constraint(equalTo: firstAssigmentLabel.leadingAnchor),
            thirdAssigmentLabel.trailingAnchor.constraint(equalTo: firstAssigmentLabel.trailingAnchor),
            
            fourthAssigmentLabel.topAnchor.constraint(equalTo: thirdAssigmentLabel.bottomAnchor, constant: 20),
            fourthAssigmentLabel.leadingAnchor.constraint(equalTo: firstAssigmentLabel.leadingAnchor),
            fourthAssigmentLabel.trailingAnchor.constraint(equalTo: firstAssigmentLabel.trailingAnchor)
        ])
    }
    
    @objc
    func didTapOnAddChildGoalButton() {
        delegate?.didTapOnAddChildGoalButton()
    }
    
    @objc
    func didTapOnAddChildAssigmentButton() {
        delegate?.didTapOnAddAssigment()
    }
    
    @objc
    func didTapOnFirstAssigmentButton() {
        delegate?.didTapOnEditAssigment()
    }
    
    @objc
    func didTapOnAssigment() {
        delegate?.didTapOnAssigment()
    }
    
    @objc
    func didTapOnConfirmTaskFinished() {
        delegate?.didTapOnConfirmTaskFinished()
    }
    
    @objc
    func didTapOnEditGoal() {
        delegate?.didTapOnEditGoal()
    }
    
    @objc
    func didTapOnGainGoal() {
        delegate?.didTapOnGainGoal()
    }
}
