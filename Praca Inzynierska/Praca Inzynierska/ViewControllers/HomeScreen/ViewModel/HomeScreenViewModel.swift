//
//  HomeScreenViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 12/06/2021.
//

import Foundation

protocol HomeScreenViewModel: AnyObject {
    
}

final class HomeScreenViewModelImplementation: BaseViewModel {
    
    override init() {
        
    }
}

extension HomeScreenViewModelImplementation: HomeScreenViewModel {
    
}
