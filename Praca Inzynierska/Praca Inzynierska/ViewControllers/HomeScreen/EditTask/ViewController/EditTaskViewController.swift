//
//  EditTaskViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 19/06/2021.
//

import UIKit

final class EditTaskViewController: BaseViewController {
    weak var coordinatorDelegate: HomeScreenCoordinatorProtocol?
    private var homeView: EditTaskView?
    var viewModel: EditTaskViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = EditTaskView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
}

private extension EditTaskViewController {
    
}

extension EditTaskViewController: EditTaskViewDelegate {
    
}
