//
//  EditTaskViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 19/06/2021.
//

import Foundation

protocol EditTaskViewModel: AnyObject {
    
}

final class EditTaskViewModelImplementation: BaseViewModel {
    
    override init() {
        
    }
}

extension EditTaskViewModelImplementation: EditTaskViewModel {
    
}
