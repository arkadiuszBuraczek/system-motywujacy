//
//  EditTaskView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 19/06/2021.
//

import UIKit

protocol EditTaskViewDelegate: AnyObject {
    
}

final class EditTaskView: UIView {
    weak var delegate: EditTaskViewDelegate?
    
    private let title = UILabel()
    private let taskNameTextFieldDescription = UILabel()
    private let taskNameTextField = UITextField()
    private let taskPointsTextFieldDescription = UILabel()
    private let taskPointsTextField = UITextField()
    private let createTaskButton = UIButton()
    private let renewLabel = UILabel()
    private let renewSwitch = UISwitch()
    private let renewDescription = UILabel()
    
    convenience init() {
        self.init(frame: .zero)
        
        configureView()
    }
}

private extension EditTaskView {
    func configureView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureTaskNameTextField()
        configureTaskPointsTextField()
        configureCreateTaskButton()
        configureRenewLabel()
        configureRenewDescription()
    }
    
    func configureTitle() {
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.text = "Edytuj zadanie!"
        title.font = .font(with: .semibold, size: .normal)
        title.numberOfLines = .zero
        title.textAlignment = .center
        
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 120),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    func configureTaskNameTextField() {
        addSubview(taskNameTextField)
        addSubview(taskNameTextFieldDescription)
        
        taskNameTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        taskNameTextField.translatesAutoresizingMaskIntoConstraints = false
        
        taskNameTextFieldDescription.text = "Nazwa zadania"
        taskNameTextFieldDescription.textColor = BaseColor.textGray
        taskNameTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        taskNameTextField.borderStyle = .roundedRect
        taskNameTextField.text = "Posprzątać biurko"
        
        NSLayoutConstraint.activate([
            taskNameTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            taskNameTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            taskNameTextField.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 36),
            
            taskNameTextFieldDescription.leadingAnchor.constraint(equalTo: taskNameTextField.leadingAnchor, constant: 4),
            taskNameTextFieldDescription.bottomAnchor.constraint(equalTo: taskNameTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureTaskPointsTextField() {
        addSubview(taskPointsTextField)
        addSubview(taskPointsTextFieldDescription)
        
        taskPointsTextFieldDescription.translatesAutoresizingMaskIntoConstraints = false
        taskPointsTextField.translatesAutoresizingMaskIntoConstraints = false
        
        taskPointsTextFieldDescription.text = "Liczba punktów do zdobycia"
        taskPointsTextFieldDescription.textColor = BaseColor.textGray
        taskPointsTextFieldDescription.font = .font(with: .regular, size: .superSmall)
        
        taskPointsTextField.borderStyle = .roundedRect
        taskPointsTextField.placeholder = "5"
        taskPointsTextField.text = "5"
        taskPointsTextField.keyboardType = .numberPad
        
        NSLayoutConstraint.activate([
            taskPointsTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            taskPointsTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            taskPointsTextField.topAnchor.constraint(equalTo: taskNameTextField.bottomAnchor, constant: 36),
            
            taskPointsTextFieldDescription.leadingAnchor.constraint(equalTo: taskNameTextField.leadingAnchor, constant: 4),
            taskPointsTextFieldDescription.bottomAnchor.constraint(equalTo: taskPointsTextField.topAnchor, constant: -6)
        ])
    }
    
    func configureCreateTaskButton() {
        addSubview(createTaskButton)
        
        createTaskButton.titleLabel?.font = .font(with: .semibold, size: .small)
        createTaskButton.layer.cornerRadius = 8
        createTaskButton.setTitle("Potwierdź zmiany", for: .normal)
        createTaskButton.backgroundColor = BaseColor.baseButtonColor

        createTaskButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            createTaskButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            createTaskButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            createTaskButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -60),
            createTaskButton.heightAnchor.constraint(equalToConstant: 42)
        ])
    }
    
    func configureRenewLabel() {
        addSubview(renewLabel)
        addSubview(renewSwitch)
        
        renewSwitch.isOn = true
        renewSwitch.translatesAutoresizingMaskIntoConstraints = false
        renewLabel.translatesAutoresizingMaskIntoConstraints = false
        renewLabel.font = .font(with: .semibold, size: .medium)
        renewLabel.text = "Odnawialne"
        
        NSLayoutConstraint.activate([
            renewLabel.topAnchor.constraint(equalTo: taskPointsTextField.bottomAnchor, constant: 24),
            renewLabel.leadingAnchor.constraint(equalTo: taskPointsTextField.leadingAnchor),
            
            renewSwitch.centerYAnchor.constraint(equalTo: renewLabel.centerYAnchor),
            renewSwitch.trailingAnchor.constraint(equalTo: taskPointsTextField.trailingAnchor)
        ])
    }
    
    func configureRenewDescription() {
        addSubview(renewDescription)
        renewDescription.translatesAutoresizingMaskIntoConstraints = false
        
        renewDescription.font = .font(with: .regular, size: .smallerMedium)
        renewDescription.numberOfLines = .zero
        renewDescription.textAlignment = .left
        renewDescription.textColor = .lightGray
        renewDescription.text = "Oznacza, że po wykonaniu zostanie odnowiona po 24 godzinach oraz będzie gotowa w tej samej konfiguracji dla użytkownika Dziecko"
        
        NSLayoutConstraint.activate([
            renewDescription.topAnchor.constraint(equalTo: renewLabel.bottomAnchor, constant: 16),
            renewDescription.leadingAnchor.constraint(equalTo: renewLabel.leadingAnchor),
            renewDescription.trailingAnchor.constraint(equalTo: renewSwitch.trailingAnchor)
        ])
    }
}
