//
//  BaseViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 12/06/2021.
//

import Foundation

class BaseViewModel {
    
    var removeCoordinatorIfNeeded: (() -> Void)?

    deinit {
        removeCoordinatorIfNeeded?()
    }
}
