//
//  LoginViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit
import FirebaseAuth

final class LoginViewController: BaseViewController {
    
    weak var coordinatorDelegate: LoginCoordinatorProtocol?
    private var homeView: LoginView?
    var viewModel: LoginViewModel?
    
    init() {
        super.init(nibName: nil, bundle: nil)        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = LoginView()
        mainView.delegate = self
        homeView = mainView
        view = mainView
    }
    
    deinit {
        print("Did deinit login view controller")
    }
}

// MARK: - LoginViewDelegate
extension LoginViewController: LoginViewDelegate {
    func didTapOnLogin(with login: String, password: String) {
        Auth.auth().signIn(withEmail: login, password: password) { [weak self] (authResult, error) in
            if let error = error as NSError? {
                switch AuthErrorCode(rawValue: error.code) {
                case .operationNotAllowed:
                    self?.handleOperationNotAllowed()
                // Error: Indicates that email and password accounts are not enabled. Enable them in the Auth section of the Firebase console.
                case .userDisabled:
                    self?.handleUserDisabled()
                // Error: The user account has been disabled by an administrator.
                case .wrongPassword:
                    self?.showWrongPasswordAlert()
                case .invalidEmail:
                    self?.handleInvalidEmail()
                // Error: Indicates the email address is malformed.
                default: ()
                }
            } else {
                self?.coordinatorDelegate?.popToRoot()
            }
        }
    }
    
    func handleInvalidEmail() {
        
    }
    
    func handleOperationNotAllowed() {
        
    }
    
    func handleUserDisabled() {
        
    }
    
    func userDataValidationFailed() {
        showValidationFailedAlert()
    }
    
    func didTapOnRegister() {
        coordinatorDelegate?.navigateToRegister()
    }
}

// MARK: - Private methods
private extension LoginViewController {
    func configure() {
        title = NSLocalizedString("login", comment: "")
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func showValidationFailedAlert() {
        let alert = UIAlertController(title: NSLocalizedString("login_validation_failed_alert_title", comment: ""),
                                      message: NSLocalizedString("login_validation_failed_alert_message", comment: ""),
                                      preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: NSLocalizedString("confirm", comment: ""),
                                          style: .cancel,
                                          handler: nil)
        alert.addAction(confirmButton)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showWrongPasswordAlert() {
        let alert = UIAlertController(title: NSLocalizedString("login_validation_failed_alert_title", comment: ""),
                                      message: NSLocalizedString("login_wrong_password", comment: ""),
                                      preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: NSLocalizedString("confirm", comment: ""),
                                          style: .cancel,
                                          handler: nil)
        alert.addAction(confirmButton)
        
        present(alert, animated: true, completion: nil)
    }
}
