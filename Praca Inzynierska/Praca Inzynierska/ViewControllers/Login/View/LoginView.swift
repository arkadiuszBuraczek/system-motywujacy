//
//  LoginView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit

protocol LoginViewDelegate: AnyObject {
    func didTapOnRegister()
    func didTapOnLogin(with login: String, password: String)
    func userDataValidationFailed()
}

final class LoginView: UIView {
    
    weak var delegate: LoginViewDelegate?
    
    private let logo = UIImageView(image: UIImage(systemName: "checkmark.seal.fill"))
    private let emailTextFieldInfoLabel = UILabel()
    private let emailTextField = UITextField()
    private let passwordTextFieldInfoLabel = UILabel()
    private let passwordTextField = UITextField()
    private let showPasswordButton = UIButton()
    private let loginButton = UIButton()
    private let registerButton = UIButton()
    private let registerButtonInfoLabel = UILabel()
    
    private var shouldShowPassword = false
    
    convenience init() {
        self.init(frame: .zero)
        
        configureView()
    }
}

// MARK: - Private methods
private extension LoginView {
    func configureView() {
        backgroundColor = BaseColor.background
        configureLogo()
        configureEmailTextField()
        configurePasswordTextField()
        configureLoginButton()
        configureRegisterButton()
    }
    
    func configureLogo() {
        addSubview(logo)
        logo.translatesAutoresizingMaskIntoConstraints = false
        logo.contentMode = .scaleAspectFill
        
        NSLayoutConstraint.activate([
            logo.topAnchor.constraint(equalTo: topAnchor, constant: Const.Constraints.logo.top),
            logo.centerXAnchor.constraint(equalTo: centerXAnchor),
            logo.widthAnchor.constraint(equalToConstant: Const.Size.logo.width),
            logo.heightAnchor.constraint(equalToConstant: Const.Size.logo.height)
        ])
    }
    
    func configureEmailTextField() {
        addSubview(emailTextField)
        addSubview(emailTextFieldInfoLabel)
        addSubview(showPasswordButton)
        
        showPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        emailTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        
        emailTextFieldInfoLabel.text = NSLocalizedString("email", comment: "")
        emailTextFieldInfoLabel.textColor = BaseColor.textGray
        emailTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        emailTextField.borderStyle = .roundedRect
        
        
        NSLayoutConstraint.activate([
            emailTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.Constraints.emailTextField.left),
            emailTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Const.Constraints.emailTextField.right),
            emailTextField.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: Const.Constraints.emailTextField.top),
            
            emailTextFieldInfoLabel.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor, constant: Const.Constraints.infoLabel.left),
            emailTextFieldInfoLabel.bottomAnchor.constraint(equalTo: emailTextField.topAnchor, constant: -Const.Constraints.infoLabel.bottom)
        ])
    }
    
    func configurePasswordTextField() {
        addSubview(passwordTextField)
        addSubview(passwordTextFieldInfoLabel)
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        
        passwordTextFieldInfoLabel.text = NSLocalizedString("password", comment: "")
        passwordTextFieldInfoLabel.textColor = BaseColor.textGray
        passwordTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        
        showPasswordButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        showPasswordButton.imageView?.contentMode = .scaleAspectFit
        showPasswordButton.addTarget(self, action: #selector(didTapOnShowPassword), for: .touchUpInside)

        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: Const.Constraints.passwordTextField.top),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.Constraints.passwordTextField.left),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Const.Constraints.passwordTextField.right),
            
            passwordTextFieldInfoLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: Const.Constraints.infoLabel.left),
            passwordTextFieldInfoLabel.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -Const.Constraints.infoLabel.bottom),
            
            showPasswordButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -Const.Constraints.showPasswordButton.left),
            showPasswordButton.centerYAnchor.constraint(equalTo: passwordTextFieldInfoLabel.centerYAnchor),
            showPasswordButton.widthAnchor.constraint(equalToConstant: Const.Size.showPasswordButton.width),
            showPasswordButton.heightAnchor.constraint(equalToConstant: Const.Size.showPasswordButton.height)
        ])
    }
    
    func configureLoginButton() {
        addSubview(loginButton)
        
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.titleLabel?.font = .font(with: .semibold, size: .small)
        loginButton.backgroundColor = BaseColor.baseButtonColor
        loginButton.layer.cornerRadius = Const.CornerRadius.button
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.setTitle(NSLocalizedString("log_in", comment: ""), for: .normal)
        loginButton.addTarget(self, action: #selector(didTapOnLoginButton), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: Const.Constraints.loginButton.top),
            loginButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            loginButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: Const.Multipier.button),
            loginButton.heightAnchor.constraint(equalToConstant: Const.Size.button.height)
        ])
    }
    
    func configureRegisterButton() {
        addSubview(registerButton)
        addSubview(registerButtonInfoLabel)
        
        registerButtonInfoLabel.font = .font(with: .regular, size: .smallMedium)
        registerButtonInfoLabel.textColor = BaseColor.textBlack
        registerButtonInfoLabel.text = NSLocalizedString("dont_have_an_account", comment: "")
        
        registerButton.setTitleColor(BaseColor.textBlack, for: .normal)
        registerButton.titleLabel?.font = .font(with: .semibold, size: .small)
        registerButton.backgroundColor = BaseColor.backgroundLight
        registerButton.layer.cornerRadius = Const.CornerRadius.button
        registerButton.setTitle(NSLocalizedString("register", comment: ""), for: .normal)
        registerButton.addTarget(self, action: #selector(didTapOnRegisterButton), for: .touchUpInside)
        
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        registerButtonInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            registerButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            registerButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: Const.Multipier.button),
            registerButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Const.Constraints.registerButton.bottom),
            registerButton.heightAnchor.constraint(equalToConstant: Const.Size.button.height),
            
            registerButtonInfoLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            registerButtonInfoLabel.bottomAnchor.constraint(equalTo: registerButton.topAnchor, constant: -Const.Constraints.registerButtonInfoLabel.bottom)
        ])
    }
    
    func validateIsUserCanLogIn() {
        if emailTextField.text?.count ?? .zero > .zero,
           passwordTextField.text?.count ?? .zero >= Const.Password.numberOfCharacters.min,
           passwordTextField.text?.count ?? .zero <= Const.Password.numberOfCharacters.max {
            delegate?.didTapOnLogin(with: emailTextField.text ?? "", password: passwordTextField.text ?? "")
        } else {
            delegate?.userDataValidationFailed()
        }
    }
    
    // MARK: - Selectors
    
    @objc
    func didTapOnShowPassword() {
        shouldShowPassword.toggle()
        
        let image = shouldShowPassword ? UIImage(systemName: "eye.slash.fill") : UIImage(systemName: "eye.fill")
        showPasswordButton.setImage(image, for: .normal)
        passwordTextField.isSecureTextEntry = !shouldShowPassword
    }
    
    @objc
    func didTapOnLoginButton() {
        validateIsUserCanLogIn()
    }
    
    @objc
    func didTapOnRegisterButton() {
        delegate?.didTapOnRegister()
    }
}

// MARK: Constants
private extension LoginView {
    struct Const {
        struct Constraints {
            static let logo: UIEdgeInsets = .init(top: 150, left: 0, bottom: 0, right: 0)
            static let emailTextField: UIEdgeInsets = .init(top: 48, left: 24, bottom: 0, right: 24)
            static let infoLabel: UIEdgeInsets = .init(top: 0, left: 4, bottom: 6, right: 0)
            static let passwordTextField: UIEdgeInsets = .init(top: 36, left: 24, bottom: 0, right: 24)
            static let showPasswordButton: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 4)
            static let loginButton: UIEdgeInsets = .init(top: 44, left: 0, bottom: 0, right: 0)
            static let registerButton: UIEdgeInsets = .init(top: 0, left: 0, bottom: 60, right: 0)
            static let registerButtonInfoLabel: UIEdgeInsets = .init(top: 0, left: 0, bottom: 8, right: 0)
        }
        
        struct Size {
            static let logo: CGSize = .init(width: 75, height: 75)
            static let showPasswordButton: CGSize = .init(width: 22, height: 22)
            static let button: CGSize = .init(width: 0, height: 42)
        }
        
        struct CornerRadius {
            static let button: CGFloat = 8
        }
        
        struct Multipier {
            static let button: CGFloat = 0.7
        }
        
        struct Password {
            static let numberOfCharacters: (min: Int, max: Int) = (6, 12)
        }
    }
}
