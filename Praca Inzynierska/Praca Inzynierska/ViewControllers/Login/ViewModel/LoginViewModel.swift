//
//  LoginViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import Foundation

protocol LoginViewModel: AnyObject {
    
}

final class LoginViewModelImplementation: BaseViewModel {
    
    override init() {
        
    }
}

extension LoginViewModelImplementation: LoginViewModel {
    
}
