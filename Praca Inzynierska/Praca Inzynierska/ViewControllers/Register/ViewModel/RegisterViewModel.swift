//
//  RegisterViewModel.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import Foundation

protocol RegisterViewModel: AnyObject {
    
}

final class RegisterViewModelImplementation: RegisterViewModel {
    
    var removeCoordinatorIfNeeded: (() -> Void)?
    
    init() {
        
    }
    
    deinit {
        removeCoordinatorIfNeeded?()
    }
}
