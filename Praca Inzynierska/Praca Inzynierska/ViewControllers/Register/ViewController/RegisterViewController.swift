//
//  RegisterViewController.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit
import FirebaseAuth

final class RegisterViewController: BaseViewController {
    
    weak var coordinatorDelegate: RegisterCoordinatorProcol?
    
    private var homeView: RegisterView?
    private weak var viewModel: RegisterViewModel?
    
    init(with viewModel: RegisterViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let mainView = RegisterView()
        mainView.delegate = self
        self.homeView = mainView
        view = mainView
    }
}

// MARK: - RegisterViewDelegate
extension RegisterViewController: RegisterViewDelegate {
    func didTapOnRegister(with login: String, password: String) {
        Auth.auth().createUser(withEmail: login, password: password) { [weak self] authResult, error in
            if let error = error as NSError? {
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed: ()
              // Error: The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.
                self?.handleOperationNotAllowed()
            case .emailAlreadyInUse: ()
              // Error: The email address is already in use by another account.
                self?.handleEmailAlreadyInUse()
            case .invalidEmail: ()
              // Error: The email address is badly formatted.
                self?.handleInvalidEmail()
            case .weakPassword: ()
              // Error: The password must be 6 characters long or more.
                self?.handlePasswordToWeak()
            default:
                print("Error: \(error.localizedDescription)")
            }
          } else {
            self?.showRegisterSuccessAlert()
          }
        }
    }
    
    func handleOperationNotAllowed() {
        
    }
    
    func handlePasswordToWeak() {
        
    }
    
    func handleInvalidEmail() {
        
    }
    
    func handleEmailAlreadyInUse() {
        
    }
    
    func userDataValidationFailed() {
        showRegisterFailedAlert()
    }
}

// MARK: Private methods
private extension RegisterViewController {
    func configure() {
        title = NSLocalizedString("register", comment: "")
    }
    
    func showRegisterSuccessAlert() {
        let name = Auth.auth().currentUser?.email ?? ""
        let alert = UIAlertController(title: NSLocalizedString("register_alert_title_success", comment: ""),
                                      message: String(format: NSLocalizedString("register_alert_message_success", comment: ""), name),
                                      preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: NSLocalizedString("confirm", comment: ""),
                                          style: .cancel,
                                          handler: { [weak self] _ in
                                            self?.dismiss(animated: true, completion: nil)
                                          })
        alert.addAction(confirmButton)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showRegisterFailedAlert() {
        let alert = UIAlertController(title: NSLocalizedString("register_alert_title", comment: ""),
                                      message: NSLocalizedString("register_alert_message", comment: ""),
                                      preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: NSLocalizedString("confirm", comment: ""),
                                          style: .cancel,
                                          handler: nil)
        alert.addAction(confirmButton)
        
        present(alert, animated: true, completion: nil)
    }
}
