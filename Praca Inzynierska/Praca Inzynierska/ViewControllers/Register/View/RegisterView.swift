//
//  RegisterView.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit

protocol RegisterViewDelegate: AnyObject {
    func didTapOnRegister(with login: String, password: String)
    func userDataValidationFailed()
}

final class RegisterView: UIView {
    
    weak var delegate: RegisterViewDelegate?
    
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let emailTextFieldInfoLabel = UILabel()
    private let emailTextField = UITextField()
    private let passwordTextFieldInfoLabel = UILabel()
    private let passwordTextField = UITextField()
    private let showPasswordButton = UIButton()
    private let registerButton = UIButton()
    
    private var shouldShowPassword = false

    convenience init() {
        self.init(frame: .zero)
        
        configureView()
    }
}

// MARK: - Private methods
private extension RegisterView {
    func configureView() {
        backgroundColor = BaseColor.background
        configureTitle()
        configureDescriptionLabel()
        configureEmailTextField()
        configurePasswordTextField()
        configureRegisterButton()
    }
    
    func configureTitle() {
        addSubview(titleLabel)
        titleLabel.text = NSLocalizedString("register", comment: "")
        titleLabel.font = .font(with: .bold, size: .normal)
        titleLabel.textColor = .black
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Const.Constraint.titleLabel.top),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    func configureDescriptionLabel() {
        addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        descriptionLabel.text = NSLocalizedString("register_description", comment: "")
        descriptionLabel.textColor = BaseColor.textGray
        descriptionLabel.font = .font(with: .regular, size: .small)
        descriptionLabel.numberOfLines = .zero
        descriptionLabel.textAlignment = .left
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Const.Constraint.descriptionLabel.top),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.Constraint.descriptionLabel.left),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Const.Constraint.descriptionLabel.right),
        ])
    }
    
    func configureEmailTextField() {
        addSubview(emailTextField)
        addSubview(emailTextFieldInfoLabel)
        addSubview(showPasswordButton)
        
        showPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        emailTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        
        emailTextFieldInfoLabel.text = NSLocalizedString("email", comment: "")
        emailTextFieldInfoLabel.textColor = BaseColor.textGray
        emailTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        emailTextField.borderStyle = .roundedRect
        emailTextField.font = .font(with: .regular, size: .small)
        emailTextField.placeholder = NSLocalizedString("register_email_placeholder", comment: "")
        
        NSLayoutConstraint.activate([
            emailTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.Constraint.emailTextField.left),
            emailTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Const.Constraint.emailTextField.right),
            emailTextField.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Const.Constraint.emailTextField.top),
            
            emailTextFieldInfoLabel.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor, constant: Const.Constraint.infoLabel.left),
            emailTextFieldInfoLabel.bottomAnchor.constraint(equalTo: emailTextField.topAnchor, constant: -Const.Constraint.infoLabel.bottom)
        ])
    }
    
    func configurePasswordTextField() {
        addSubview(passwordTextField)
        addSubview(passwordTextFieldInfoLabel)
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextFieldInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        
        passwordTextFieldInfoLabel.text = NSLocalizedString("password", comment: "")
        passwordTextFieldInfoLabel.textColor = BaseColor.textGray
        passwordTextFieldInfoLabel.font = .font(with: .regular, size: .superSmall)
        
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = .font(with: .regular, size: .small)
        passwordTextField.placeholder = NSLocalizedString("register_password_placeholder", comment: "")
        
        showPasswordButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        showPasswordButton.imageView?.contentMode = .scaleAspectFit
        showPasswordButton.addTarget(self, action: #selector(didTapOnShowPassword), for: .touchUpInside)

        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: Const.Constraint.passwordTextField.top),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.Constraint.passwordTextField.left),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Const.Constraint.passwordTextField.right),
            
            passwordTextFieldInfoLabel.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor, constant: Const.Constraint.infoLabel.left),
            passwordTextFieldInfoLabel.bottomAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: -Const.Constraint.infoLabel.bottom),
            
            showPasswordButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor, constant: -Const.Constraint.showPasswordButton.left),
            showPasswordButton.centerYAnchor.constraint(equalTo: passwordTextFieldInfoLabel.centerYAnchor),
            showPasswordButton.widthAnchor.constraint(equalToConstant: Const.Size.showPasswordButton.width),
            showPasswordButton.heightAnchor.constraint(equalToConstant: Const.Size.showPasswordButton.height)
        ])
    }
    
    func configureRegisterButton() {
        addSubview(registerButton)
        
        registerButton.setTitleColor(BaseColor.textBlack, for: .normal)
        registerButton.titleLabel?.font = .font(with: .semibold, size: .small)
        registerButton.backgroundColor = BaseColor.backgroundLight
        registerButton.layer.cornerRadius = Const.CornerRadius.button
        registerButton.setTitle(NSLocalizedString("register", comment: ""), for: .normal)
        registerButton.addTarget(self, action: #selector(didTapOnRegisterButton), for: .touchUpInside)
        
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            registerButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            registerButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: Const.Multipier.button),
            registerButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: Const.Constraint.registerButton.top),
            registerButton.heightAnchor.constraint(equalToConstant: Const.Size.button.height)
        ])
    }
    
    func validateIfUserCanBeRegistered() {
        if emailTextField.text?.count ?? .zero > .zero,
           passwordTextField.text?.count ?? .zero >= Const.Password.numberOfCharacters.min,
           passwordTextField.text?.count ?? .zero <= Const.Password.numberOfCharacters.max {
            delegate?.didTapOnRegister(with: emailTextField.text ?? "", password: passwordTextField.text ?? "")
        } else {
            delegate?.userDataValidationFailed()
        }
    }
    
    // MARK: - Selectors
    
    @objc
    func didTapOnShowPassword() {
        shouldShowPassword.toggle()
        
        let image = shouldShowPassword ? UIImage(systemName: "eye.slash.fill") : UIImage(systemName: "eye.fill")
        showPasswordButton.setImage(image, for: .normal)
        passwordTextField.isSecureTextEntry = !shouldShowPassword
    }
    
    @objc
    func didTapOnRegisterButton() {
        validateIfUserCanBeRegistered()
    }
}

// MARK: Constants
private extension RegisterView {
    struct Const {
        struct Constraint {
            static let titleLabel: UIEdgeInsets = .init(top: 24, left: 0, bottom: 0, right: 0)
            static let showPasswordButton: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 4)
            static let emailTextField: UIEdgeInsets = .init(top: 48, left: 24, bottom: 0, right: 24)
            static let passwordTextField: UIEdgeInsets = .init(top: 36, left: 24, bottom: 0, right: 24)
            static let infoLabel: UIEdgeInsets = .init(top: 0, left: 4, bottom: 6, right: 0)
            static let descriptionLabel: UIEdgeInsets = .init(top: 48, left: 24, bottom: 0, right: 24)
            static let registerButton: UIEdgeInsets = .init(top: 48, left: 0, bottom: 0, right: 0)
        }
        
        struct Multipier {
            static let button: CGFloat = 0.7
        }
        
        struct Size {
            static let showPasswordButton: CGSize = .init(width: 22, height: 22)
            static let button: CGSize = .init(width: 0, height: 42)
        }
        
        struct CornerRadius {
            static let button: CGFloat = 8
        }
        
        struct Password {
            static let numberOfCharacters: (min: Int, max: Int) = (6, 12)
        }
    }
}
