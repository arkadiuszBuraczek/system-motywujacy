//
//  ArrayExtension.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 09/06/2021.
//

import Foundation

extension Array {
    mutating func removeElements(_ predicate: (_ element: Element) -> Bool) {
        self = self.filter{ !predicate($0) }
    }
}
