//
//  UIFontExtension.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 08/06/2021.
//

import UIKit

extension UIFont {
    enum Size: CGFloat {
        /// 10
        case superSmall = 10.0
        /// 12
        case smaller = 12.0
        /// 13
        case smallerMedium = 13.0
        /// 14
        case small = 14.0
        /// 15
        case smallMedium = 15.0
        /// 16
        case medium = 16.0
        /// 17
        case bigMedium = 17.0
        /// 18
        case normal = 18.0
        /// 20
        case big = 20.0
        /// 22
        case large = 22.0
        /// 24
        case huge = 24.0
        /// 28
        case extraHuge = 28.0

        var size: CGFloat {
            return self.rawValue
        }
    }

    static func font(with type: Weight = .regular, size: Size = .medium) -> UIFont {
        return .systemFont(ofSize: size.size, weight: type)
    }
}
