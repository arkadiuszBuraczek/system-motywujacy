//
//  AppDelegate.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 22/04/2021.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var appCoordinator: AppCoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        appCoordinator = AppCoordinator(window: window)

        appCoordinator?.start()
        
        window?.makeKeyAndVisible()

        return true
    }
}

