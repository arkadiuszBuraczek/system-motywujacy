//
//  Task.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 21/06/2021.
//

import Foundation

struct Task: Codable {
    
    let done: Bool
    let finishDate: String
    let name: String
    let renew: Bool
    let value: Int
}
