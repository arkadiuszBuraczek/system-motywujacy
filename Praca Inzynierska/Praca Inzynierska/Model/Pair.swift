//
//  Pair.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 21/06/2021.
//

import Foundation
import Firebase

struct Pair {
    init(createDate: String, goals: [Goal], id: String, name: String, tasks: [Task], users: [User], password: String) {
        self.createDate = createDate
        self.goals = goals
        self.id = id
        self.name = name
        self.tasks = tasks
        self.users = users
        self.password = password
    }
    
    init?(snapshot: DataSnapshot) {
      guard
        let value = snapshot.value as? [String: AnyObject],
        let createDate = value["createDate"] as? String,
        let goals = value["goals"] as? [Goal],
        let id = value["id"] as? String,
        let name = value["name"] as? String,
        let tasks = value["tasks"] as? [Task],
        let users = value["users"] as? [User],
        let password = value["password"] as? String
      else {
        return nil
      }
      
        self.createDate = createDate
        self.goals = goals
        self.id = id
        self.name = name
        self.tasks = tasks
        self.users = users
        self.password = password
    }
    
    let password: String
    let createDate: String
    var goals: [Goal]
    let id: String
    let name: String
    let tasks: [Task]
    let users: [User]
}
