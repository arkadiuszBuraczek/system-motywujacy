//
//  User.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 21/06/2021.
//

import Foundation

struct User: Codable {
    let name: String
    let parent: Bool
}
