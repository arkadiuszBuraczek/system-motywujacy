//
//  Goal.swift
//  Praca Inzynierska
//
//  Created by Arkadiusz Buraczek on 21/06/2021.
//

import Foundation

struct Goal: Codable {
    let currentValue: Int?
    let goalValue: Int?
    let name: String?
}
